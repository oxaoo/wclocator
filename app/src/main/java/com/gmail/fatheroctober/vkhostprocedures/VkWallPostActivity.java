package com.gmail.fatheroctober.vkhostprocedures;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.gmail.yol0317.wclocator.MainActivity;
import com.gmail.yol0317.wclocator.R;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import java.io.IOException;

public class VkWallPostActivity extends ActionBarActivity {

    ImageButton wallPrint;
    Button noThanksButton;

    private void showError(VKError error) {
        new AlertDialog.Builder( this)
                .setMessage(error.errorMessage)
                .setPositiveButton("OK", null)
                .show();

        if (error.httpError != null) {
            Log.w("Test", "Error in request or upload", error.httpError);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vk_wall_post);
        Toast.makeText(this, "Success teleportation", Toast.LENGTH_SHORT).show();
        wallPrint = (ImageButton) findViewById(R.id.buttonPrint);
        noThanksButton = (Button) findViewById(R.id.buttonNo);

        View.OnClickListener wallPrintListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendAchivmentToWall();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        View.OnClickListener backOnMainActivity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity();
            }
        };

        wallPrint.setOnClickListener(wallPrintListener);
        noThanksButton.setOnClickListener(backOnMainActivity);
    }

    private void startMainActivity()
    {
        startActivity(new Intent(this, MainActivity.class));
    }



    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vk_wall_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void makePost(VKAttachments attachments) throws  Exception{

        makePost(attachments, null);
    }

    private void sendAchivmentToWall() throws Exception
    {
        final Bitmap wallWc;
        try {
            wallWc = BitmapFactory.decodeResource(this.getResources(), R.drawable.wallprint);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw new Exception("Exception with wall print img");
        }
        VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(wallWc, VKImageParameters.jpgImage(0.9f)), new Integer(VKSdk.getAccessToken().userId), 0);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                wallWc.recycle();
                VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);
                try {
                    makePost(new VKAttachments(photoModel),"New achievment. I discover new location with WClocator");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VKError error) {
                showError(error);
            }
        });
    }

    private void makePost(VKAttachments attachments, String message) throws Exception{
        System.out.println("ID USER ="+VKSdk.getAccessToken().userId);
        VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.USER_ID, "-"+VKSdk.getAccessToken().userId, VKApiConst.ATTACHMENTS, attachments, VKApiConst.MESSAGE, message));
        post.setModelClass(VKWallPostResult.class);
        post.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKWallPostResult result = (VKWallPostResult)response.parsedModel;
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://vk.com/wall"+VKSdk.getAccessToken().userId, result.post_id)) );
                startActivity(i);
            }

            @Override
            public void onError(VKError error) {
                showError(error.apiError != null ? error.apiError : error);
            }
        });
    }
}
